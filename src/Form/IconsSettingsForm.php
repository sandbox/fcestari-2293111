<?php

namespace Drupal\languageicons\Form;
 
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Form\ConfigFormBase;

class IconsSettingsForm extends ConfigFormBase {

  public function getFormId() {
    return 'languageicons_settings';
  }
 
  public function buildForm(array $form, array &$form_state) {
    $config = $this->config('languageicons.settings');

    $form['placement'] = array(
      '#type' => 'radios',
      '#title' => t('Icon placement'),
      '#options' => array(
        'before' => t('Before link'),
        'after' => t('After link'),
        'replace' => t('Replace link'),
      ),
      '#default_value' => $config->get('placement'),
      '#description' => t('Where to display the icon, relative to the link title.'),
    );

    $form['default_css'] = array(
      '#type' => 'checkbox',
      '#title' => t('Load default css file.'),
      '#default_value' => $config->get('default_css'),
    );
    
    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, array &$form_state) {
    $config = $this->config('languageicons.settings');

    foreach($form_state['values'] as $key => $value) {
      $config->set($key, $value);
    }

    $config->save();

    return parent::submitForm($form, $form_state);
  }
}