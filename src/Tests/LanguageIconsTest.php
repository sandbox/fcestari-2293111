<?php

/**
 * @file
 * Definition of Drupal\languageicons\Tests\LanguageIconsTest.
 */

namespace Drupal\languageicons\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Test the user-facing menus in Block Example.
 *
 * @ingroup languageicons
 */
class LanguageIconsTest extends WebTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('language', 'languageicons');
  
  protected $WebUser;
  
  /**
   * The installation profile to use with this test.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Language Icons Test',
      'description' => 'Test admin URL for Language Icons.',
      'group' => 'LanguageIcons',  
    );
  }
  
  /**
   * Enable modules and create user with specific permissions.
   */
  public function setUp() {
    parent::setUp();
    // Create user.
    $this->WebUser = $this->drupalCreateUser(array('administer languages'));
  }
  
  /**
   * Tests languageicons.
   */
  
  public function testLanguageIcons() {
    // Login the admin user.
    $this->drupalLogin($this->WebUser);
    $theme_name = \Drupal::config('system.theme')->get('default');
    
    // Checks the module exists
    $this->assertTrue(module_exists('languageicons'));
    
    // Access to admin page
    $this->drupalGet('admin/config/regional/language/icons');
    $this->assertResponse(200, 'Description page exists.');
    
    // Test the add tab.edit-submit
    // Add the new entry.
    $this->drupalPostForm(
        'admin/config/regional/language/icons',
        array(
            'placement' => 'after',
            'default_css' => false,
        ),
        t('Save configuration')
    );
    
  }

}
